from obspy import UTCDateTime
#from obspy.clients.fdsn import Client
from obspy.clients.iris import Client
import os
class Responses():
	def main(self, network,  station, channel, year,  month1, month2, day1, day2):
		# Crear un objeto de cliente FDSN
		client = Client()

		# Especificar la estación, red y canal de interés
		#network = "ZA"
		#station = "ALPI"
		location = "30"
		channel = channel+"Z"

		# Especificar el rango de tiempo de febrero de 2006 a diciembre de 2006
		#start_time = UTCDateTime("2006-02-01T00:00:00")
		#end_time = UTCDateTime("2006-12-31T23:59:59")
		t1=year+"-"+month1+"-"+day1+"T00:00:00"
		t2="2007"+"-"+month2+"-"+day2+"T23:59:59"

		start_time = UTCDateTime(t1)
		end_time = UTCDateTime(t2)
		print(start_time)
		print(end_time)
		try:
			# Obtener la respuesta del instrumento utilizando el método 'get_stations'
			inventory = client.resp(network=network, station=station, location=location, channel=channel, starttime=start_time, endtime=end_time, level="response")

			# Guardar el objeto de inventario en un archivo de texto plano
			#inventory.write("ALPI_ZA_2006_inventory.txt", format="STATIONTXT")
			print(inventory.decode())
			ax_pat=os.getcwd()
			ruta_guardar_datos_end=ax_pat+"/"+network+"_RESPONSES/"
			#ruta_guardar_datos_end="/media/carlos/MEMORIA"+"/XF/"+day_j
			print (ruta_guardar_datos_end)



			if not os.path.exists(ruta_guardar_datos_end):
				os.makedirs(ruta_guardar_datos_end)


			with open(ruta_guardar_datos_end+station+"_response", "w") as file:
				file.write(str(inventory.decode()))
			print("Respuestas descargadas exitosamente y guardadas en ALPI_ZA_2006_inventory.txt")
		except Exception as e:
			print(f"Error: {e}")



inicio=Responses()
code="XF"
station="MA05"
channel="BH"
year="2006"
month1="01"
month2="09"
day1="26"
day2="09"
inicio.main(code, station, channel, year, month1, month2, day1, day2)
 

import numpy as np
from obspy import read
import matplotlib.pyplot as plt
import math
import os
import os.path
from obspy.imaging.cm import viridis
from obspy.signal.invsim import cosine_taper
from obspy.signal.util import util_geo_km
from Lectura_file import *

class FrecuenciaEsquina():
	def abrirArchivoStation(self):

		
		path_sta=self.path_sta
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		print (self.estaciones)

		#archivo =self.pathfileDF()
		#self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF


	def Frecuencia(self, path):
		
		valores=[]
		st = read(path)
		# Seleccionar una traza del conjunto de datos
		trace = st[0]

		# Parámetros para el cálculo de la frecuencia de esquina
		t_start = 0  # tiempo de inicio en segundos
		t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
		npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
		dt = 1 / trace.stats.sampling_rate

		# Calcular la transformada de Fourier
		freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
		positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
		
		# Tomar solo la parte positiva de la transformada
		freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

		# Calcular la densidad espectral de potencia
		power_spectrum = np.abs(ampl_pos) ** 2

		#plt.loglog(power_spectrum)
		#plt.show()


		# Establecer un límite inferior en la frecuencia para graficar
		lower_freq_limit = 1.0  # Ajusta según tus necesidades

		# Suavizar el power_spectrum con un filtro de media móvil
		window_size = 150  # Tamaño de la ventana del filtro de media móvil
		power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')



		# Identificar la frecuencia máxima y mínima
		max_frequency_index = np.argmax(power_spectrum_smooth)
		min_frequency_index = np.argmin(power_spectrum_smooth)

		# Convertir los índices a frecuencias en Hz
		max_frequency = freq_pos[max_frequency_index]
		min_frequency = freq_pos[min_frequency_index]



		for i in range(len(freq_pos)):
			if freq_pos[i]>= lower_freq_limit:
				indice=i
				break

		#print(indice)
		freq_pos=freq_pos[indice:len(freq_pos)]
		power_spectrum_smooth=power_spectrum_smooth[indice:len(power_spectrum_smooth)]



		# Crear listas para almacenar los valores de frecuencia y densidad espectral
		frequencies = []
		power_values = []

		# Iterar sobre los valores y almacenarlos en las listas
		for freq_value, power_value in zip(freq_pos, power_spectrum_smooth):
			frequencies.append(freq_value)
			power_values.append(power_value)

		# Convertir las listas a arrays de NumPy
		frequencies = np.array(frequencies)
		power_values = np.array(power_values)


		# Filtrar las frecuencias y el espectro de potencia suavizado según el límite inferior
		filtered_freq_pos = freq_pos[freq_pos >= lower_freq_limit]
		filtered_power_spectrum_smooth = power_spectrum_smooth[freq_pos >= lower_freq_limit]

		# Convertir las unidades del eje y a "medias dinas * cm"
		power_in_mdc = filtered_power_spectrum_smooth * 1e-2  # 1 (unidad estándar) = 1 mediana dina * cm

		# Almacenar los valores en listas
		frequencies = list(filtered_freq_pos)
		power_values = list(power_in_mdc)



		# Aplicar logaritmo a los arrays
		#log_frequencies = np.log(frequencies)
		#log_power_values = np.log(power_values)


		log_power_values=power_values
		log_frequencies=frequencies

		# Encontrar la frecuencia de esquina (índice del 90% de la potencia acumulada)
		cumulative_power = np.cumsum(power_spectrum) / np.sum(power_spectrum)
		corner_frequency_index = np.argmax(cumulative_power >= 0.8)


		# Asegurarse de que el índice esté dentro de los límites
		if corner_frequency_index < len(filtered_freq_pos):
			corner_frequency = filtered_freq_pos[corner_frequency_index]
			#print(f"Frecuencia de esquina: {corner_frequency}")
			# Convertir el índice a frecuencia en Hz
			corner_frequency = freq_pos[corner_frequency_index]
		else:
			#print("Índice de frecuencia de esquina fuera de los límites.")
			corner_frequency = 11

		

		st_crece=0
		st_decre=0
		contc=0
		contd=0



		for i in range(len(log_power_values)):
			if log_power_values[i]<= log_power_values[i+1]:

				
				if st_crece==1 and st_decre==1 and contd>150:
			
					punto_int= log_power_values[i]
					break
				else:
					st_crece=1
					contc+=1
					#print(log_power_values[i], "-- Crece" )
			else:
				st_decre=1
				contd+=1
				#print(log_power_values[i], "-- Decrece" )

				st_crece=0


		#print("punto_int ", punto_int)



		puntos_int=[]
		indice_int=[]
		fin=len(log_frequencies)
		#fin=log_frequencies[fin]
		x_aux=np.arange(0, fin, 0.0001)

		for i in range(len(log_power_values)):
				if round(log_power_values[i],2) == round(punto_int, 2):
					puntos_int.append(log_power_values[i])
					indice_int.append(i)	
		#print(puntos_int)
		#print(indice_int, "indices")
		
		x11=0
		y11=0
		x01=0
		y01=0
		am=0
		for i in (indice_int):
			#print(log_frequencies[i])
			#plt.scatter(log_frequencies[i], log_power_values[i], color='red', label='Punto (1, 2)')
			if x01==0:
				if am==0:
					Mo=log_frequencies[i-100]
					am=1
				x01=log_frequencies[i]
				y01=log_power_values[i]
				xx0=1
			x11=log_frequencies[i]
			y11=log_power_values[i]
			x0=i

		#print("x11: ", x11, " y11: ", y11)
		fin=len(log_frequencies)
		#print("valor final  ", x0)
		#print(len(log_frequencies))
		x00=0
		x=0
		y2=0
		nn=len(log_frequencies)
		for j in range(0, nn, 10):
			#print(j)
			#j=2400
			cont=y11-1
			if j>= x0:
				#print("entra al if***")
				x1=round(log_frequencies[j], 2)
				#x1=round(log_frequencies[nn-1], 2)
				y1=round(punto_int, 2)
				# Calcular la pendiente
				m = -1

				# Calcular el término independiente (b) usando la ecuación de la recta
				b = y1 - m * x1
		
				# Crear un conjunto de puntos para la recta
				x = np.linspace(cont, log_frequencies[nn-1], 55)
				y2 = m * x + b
				status_cruce=False
		
				n=len(y2)
				for jj in range(n):
			
					if status_cruce==False:
						for ji in range(len(log_power_values)):
							if log_power_values[ji]<punto_int and y2[jj]<punto_int:
								if round(y2[jj],4) == round(log_power_values[ji],4) :
									#print("con j= ", j, " se encuentran en la recta")
									status_cruce=True
									break
						
						if status_cruce == True:
							cont=cont+0.1
							break
				if status_cruce == False:
					x00=j
					break


		xi=log_frequencies[x00]
		yi=punto_int
		#print(xi, " ------ ", yi)

		#print("range is : ", log_frequencies[xx0], " ------ ", log_frequencies[x0])
		if corner_frequency <10:
			xi=round(corner_frequency, 2)
			print('frecuencia de esquina: ', round(corner_frequency, 2))
		else:
			xi=round(xi, 2)
			print("Frecuencia de esquina: ", xi)


		corner_frequency=xi
		# Calcular el momento sísmico
		momento = self.calcular_momento_sismico(corner_frequency)
		valores.append(xi)
		Mo=round(Mo, 2)
		valores.append(momento)
		return valores




		x1=corner_frequency
		
		y1= punto_int


		print(x1,  y1)

		x=35
		y=100

		# # Ángulo en grados
		# theta_deg = 135

		# # Convertir el ángulo a radianes
		# theta_rad = np.radians(theta_deg)

		# # Calcular la pendiente
		# m = np.tan(theta_rad)

		# # Crear datos para la línea
		# #x = np.linspace(6, 12, 10)
		# #y = m * (x - x1) + y1




		# Graficar en escala logarítmica con unidades modificadas en el eje y
		plt.loglog(frequencies, power_values, color='black', linestyle='--')

		# Configurar el formato del eje y con unidades modificadas
		plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: '{:.0e}'.format(x)))


		# #Graficas
		#plt.loglog(log_frequencies[freq_pos >= lower_freq_limit], log_power_values[freq_pos >= lower_freq_limit], color='black', linestyle='--')
		# Configurar el formato del eje x con unidades modificadas
		#plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: '{:.0e}'.format(y)))

		#plt.ylabel("Frecuencia (medias dinas * cm)")
		# #plt.plot(log_frequencies, log_power_values)
		# #plt.xscale('log')  # Establecer el eje x en escala logarítmica
		# #plt.yscale('log')  # Establecer el eje y en escala logarítmica
		# #plt.axvline(x=xi, color='red', linestyle='--', label=f'Frecuencia de Esquina: {xi:.2f} Hz')
		# #plt.plot(x,y2)
		# #plt.loglog(x, y, label='Línea a 135°')
		plt.plot([x1, x], [y1, y], color='black', linestyle='--')
		# #plt.axvline(x=corner_frequency, color='black', linestyle='--')
		# #plt.scatter(xi, yi, color='red', label=f'corner frecuency: {xi:.2f} Hz')
		plt.scatter(xi, yi, color='black', label=f'corner frecuency: {corner_frequency:.2f} Hz')
		# plt.scatter(Mo, yi, color='black', label=f'seismic moment: {Mo:.2f} Hz')
		# #plt.axvline(x=Mo, color='green', linestyle='--')
		plt.axhline(y=punto_int, color='black', linestyle='--')
		plt.legend()
		plt.show()
	def calcular_momento_sismico(self, freq):
		

		B_cms=3.3
		D_pascal=100		
		D_bars = 100
		B_kmps = 3.3
		#f=float(f)
		# Calcular M usando la fórmula reorganizada
			
		aux=freq / (4.91e6 * B_cms)
		aux=aux**3
		Mo = D_pascal /aux
    
		return momento_sismico


	def trabajar(self):
		self.abrirArchivoStation()
		#for e in range(len(responses)):
			#print(responses[e])
		#file="/home/carlos/Documentos/CrossQuake-Data/tremores/"
		file=self.pathCat+'/'
		#file22="/home/carlos/Documentos/CrossQuake-Data/"
		file22=os.getcwd()
		estaciones=self.path_sta
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		self.path=[]
		path11=[]
		self.promedios=[]
		self.promedios1=[]
		for i in archivos:
			ruta0=	file+i
			self.path.append(ruta0)
			path11.append(i)

		ruta_guardar_datos=file22+'/corner_frequency_'+self.identificador
		ruta_guardar_seed=file22+'/'+self.identificador+'/'
		if not os.path.exists(ruta_guardar_datos):
			os.makedirs(ruta_guardar_datos)

		ruta00=''
		for i in range(len(self.path)):
			print (self.path[i])
			ruta00=self.path[i]
			fo = open(ruta_guardar_datos+'/'+path11[i]+'.txt', 'a')
			arch = os.listdir(self.path[i])
			arch=sorted(arch)
			remove_esta=[]
			cont=0
			suma=0
			suma1=0

			for archivo in arch:
				if archivo.rfind('HHZ') != -1 or archivo.rfind('BHZ') != -1:
					print(archivo)
					for estacion in range(len(estaciones)):
						if archivo.rfind(estaciones[estacion]) != -1:
							esta=estaciones[estacion]
							break
					path=(ruta00+'/'+archivo)
					freq_v= self.Frecuencia(path)
					freq=freq_v[0]
					Mo=freq_v[1]

					

					cont=cont+1
					suma= suma+ freq
					suma1=suma1+Mo
					prom=round((suma/cont), 2)
					promMo=round((suma1/cont), 2)
					self.promedios.append(round(prom,1))
					self.promedios1.append(round(promMo, 1))
					fo.write(esta+'\t'+ str(freq)+'\t'+str(Mo)+'\n')
			fo.write('average freq:\t'+str(prom)+'\n')
			fo.write('average Mo:\t'+str(promMo))
			fo.close()
			print('\n')
			ruta00=''


		inicioseed = semilla(ruta_guardar_datos, ruta_guardar_seed)
		inicioseed.Main()
		# Crear un diccionario para almacenar las cuentas
		conteo_valores = {}

		# Contar la frecuencia de cada valor en la lista
		for valor in self.promedios:
			if valor in conteo_valores:
				conteo_valores[valor] += 1
			else:
				conteo_valores[valor] = 1

		# # Guardar los resultados en un archivo de texto
		# with open(file+"HistogramaFreq.txt", "w") as archivo:
		# 	for valor, cantidad in conteo_valores.items():
		# 		archivo.write(f"{valor}: {cantidad} \n")

		# # Crear un diccionario para almacenar las cuentas
		# conteo_valores = {}

		# # Contar la frecuencia de cada valor en la lista
		# for valor in self.promedios1:
		# 	if valor in conteo_valores:
		# 		conteo_valores[valor] += 1
		# 	else:
		# 		conteo_valores[valor] = 1

		# # Guardar los resultados en un archivo de texto
		# with open(file+"HistogramaMo.txt", "w") as archivo:
		# 	for valor, cantidad in conteo_valores.items():
		# 		archivo.write(f"{valor}: {cantidad} \n")

	def main(self):
		valor = "" #para el inicio los entry tenga b


		print("Input of data for corner frecuency")


		self.identificador=input("Input name identificador for corner frecuency: \n")
		archivo=input("Input path catalog (tremors)")
		

		try:
			if (os.path.isdir(archivo)) :
				self.pathCat=archivo
				status_df=True
		except Exception as e:
			print("Error, format incorrect")
			status_df=False
		while status_df==False:
			print("path not is a dir")
			archivo=input("Input path catalog (tremors)")
		

			try:
				if (os.path.isdir(archivo)) :
					self.pathCat=archivo
					status_df=True
			except Exception as e:
				print("Error, format incorrect")
				status_df=False


		


		self.path_sta=input("Input path file stations: \n")
		try:
			if (os.path.isfile(self.path_sta)) :
				status_sta=True
		except Exception as e:
			print("Error, format incorrect")
			status_sta=False

		while status_sta== False:
			self.path_sta=input("Input path file stations \n")

		

			try:
				if (os.path.isfile(self.path_sta)) :
					status_sta=True
			except Exception as e:
				print("Error, format incorrect")
				status_sta=False
		
		
		print("only enter the first two characters (e.g. HH, HB, LL..)")
		self.Channel=input("Input name Channel:\t  ")
		#print("only enter the first two characters (e.g. HH, HB, LL..)")
		try:
			print("Name Channel is:  ", self.Channel)
			self.Channel = self.Channel.upper()
			status_channel=True
		except Exception as e:
			print("error input name Channel")
			status_channel=False

		while status_channel == False:
			print("only enter the first two characters (e.g. HH, HB, LL..)")
			self.Channel=input("Input name Channel:\t  ")
			
			try:
				print("Name Channel is: ", self.Channel)
				self.Channel = self.Channel.upper()+"?"
				status_code=True
			except Exception as e:
				print("error input name Channel")
				status_code=False
		self.mostrarInfo()

	def mostrarInfo(self):
		os.system("clear")
		print ("information input")
		print("Path catalog master is:\n ", self.pathCat)
		print("Channel is:\n ", self.Channel)
		print("Path file station is:\n ", self.path_sta)
		print("\n *******************************")
		resp=input("right information? (Y/N)")

		if resp == "Y" or resp =="y" or resp== "yes":
			self.trabajar()
		else:
			print("1.-input information")
			print("2.-cancelar")
			choice=input("Select choice (1-2):\n")

			try:
			
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=3:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 2\n")

			except:
				os.system("clear")
				choice=0
				print("format incorrect, please enter number between 1 and 2\n")


			while choice <=0 or choice>=3:

			
				print("1.-input information")
				print("2.-cancelar")
				choice=input("Select choice (1-2):\n")
				try:
				
					choice=int(choice)
					print ("choice select: ",choice)
					if choice <=0 or choice>=3:
						os.system("clear")
						print("Choice incorrect, please select choice between 1 and 2\n")
				except:
					os.system("clear")
					choice=0
					print("Choice incorrect, please select choice between 1 and 2\n")

			if choice ==1:
				self.main()
			else:
				self.cancelar()
		
	def cancelar(self):
		os.system("clear")		

						

			
				

#inicio = FrecuenciaEsquina()
# estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
# for i in range(56):

# 	if i<9:
# 		estaciones.append('MA0'+str(i+1))
# 	else:
# 		estaciones.append('MA'+str(i+1))

# inicio.Main(estaciones)
#inicio.Frecuencia()
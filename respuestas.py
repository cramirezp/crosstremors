



class Respuestas():
	def Main(self, estacion, ruta, ruta_salve):

		fo = open(ruta_salve+'/response_'+estacion, 'a')

		fo.write('setbb f1 0.7\n')
		fo.write('setbb f2 1.4\n')
		fo.write('setbb f3 20\n')
		fo.write('setbb f4 30\n')
		fo.write('setbb respz "'+ruta+'/RESP.'+estacion+'.XF"\n')
		fo.write('setbb respe "'+ruta+'/RESP.'+estacion+'.XF"\n')
		fo.write('setbb respn "'+ruta+'/RESP.'+estacion+'.XF"\n')
		fo.write('\n')

		fo.write('r *.BHZ.*\n')
		fo.write('synch        \n')
		fo.write('qdp off\n')
		fo.write('rmean\n')
		fo.write('rtrend\n')
		fo.write('taper\n')
		fo.write('transfer from evalresp fname '+ruta+'/RESP.'+estacion+'.BHZ.XF'+' to none freq 0.7 1.4 20 30\n')
		fo.write('w over\n')
		fo.write('r *.BHN.*\n')
		fo.write('synch        \n')
		fo.write('qdp off\n')
		fo.write('rmean\n')
		fo.write('rtrend\n')
		fo.write('taper\n')
		fo.write('transfer from evalresp fname '+ruta+'/RESP.'+estacion+'.BHN.XF'+' to none freq 0.7 1.4 20 30\n')
		fo.write('w over\n')
		fo.write('r *.BHE.*\n')
		fo.write('synch        \n')
		fo.write('qdp off\n')
		fo.write('rmean\n')
		fo.write('rtrend\n')
		fo.write('taper\n')
		fo.write('transfer from evalresp fname '+ruta+'/RESP.'+estacion+'.BHE.XF'+' to none freq 0.7 1.4 20 30\n')
		fo.write('w over')



inicio=Respuestas()
estaciones=[]
for i in range(55):

	if i<9:
		estaciones.append('MA0'+str(i+1))
	else:
		estaciones.append('MA'+str(i+1))


ruta_salve='/home/carlos/Documentos/crossquake/XF/RespuestasDockerMars/'
#ruta='/crossquake/XF/Responses'
ruta='/crossquake/XF/Responses'
for i in estaciones:
	estacion=i
	inicio.Main(estacion, ruta, ruta_salve)
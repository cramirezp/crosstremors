import numpy as np
from obspy import read
import matplotlib.pyplot as plt

# Leer un archivo sismográfico (reemplaza "tus_datos.mseed" con el nombre de tu archivo)
#st = read("/home/carlos/Documentos/crossquake/ZAL/results_pre/2006.063.22/2006.063.22.07.45.MORA.ZA.HHZ.SAC")
#st = read("/home/carlos/Documentos/crossquake/ZAL/results_pre/Resultados_2006/031/09/19/2006.01.31.09.18.35.80.ZAPO.HHZ.SAC")
st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.04.08.21/2006.04.08.21.MORA.HHZ.SAC")

# Seleccionar una traza del conjunto de datos
trace = st[0]

# Parámetros para el cálculo de la frecuencia de esquina
t_start = 0  # tiempo de inicio en segundos
t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
dt = 1 / trace.stats.sampling_rate

# Calcular la transformada de Fourier
freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas

# Tomar solo la parte positiva de la transformada
freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

# Calcular la densidad espectral de potencia
power_spectrum = np.abs(ampl_pos) ** 2

# Encontrar la frecuencia de esquina (índice del 90% de la potencia acumulada)
cumulative_power = np.cumsum(power_spectrum) / np.sum(power_spectrum)
corner_frequency_index = np.argmax(cumulative_power >= 0.9)

# Convertir el índice a frecuencia en Hz
corner_frequency = freq_pos[corner_frequency_index]

print(freq_pos)
# Establecer un límite inferior en la frecuencia para graficar
lower_freq_limit = 0.01  # Ajusta según tus necesidades

# Graficar solo la porción del espectro donde comienza la información significativa
plt.loglog(freq_pos[freq_pos >= lower_freq_limit], power_spectrum[freq_pos >= lower_freq_limit], label='Densidad Espectral de Potencia')
plt.axvline(x=corner_frequency, color='red', linestyle='--', label=f'Frecuencia de Esquina: {corner_frequency:.2f} Hz')
plt.xlabel("Frecuencia (Hz)")
plt.ylabel("Densidad Espectral de Potencia")
plt.title(f"Espectro de Potencia (Parte Positiva) con Frecuencia de Esquina (Log-log) - Frecuencia Mínima: {lower_freq_limit} Hz")
plt.legend()
plt.show()
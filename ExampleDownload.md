Input year of analisys (YYYY):

    example:2006

Input FDSN code:

    example: za

Input initial month (mm):

    example: 3   (No. of month where data exists)

Input initial day (dd)

    example: 29 (Download information from the initial day to the final day of the selected month)

Input path file stations

    example: /additional-information/examples/stations.txt (file with names of stations)

right information? (Y/N)
    example Y (N.- requests to enter the previous data again)

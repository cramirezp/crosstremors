To run CrossTremors you can do it from a Docker container

1.- Install  Docker

    https://www.docker.com/

2.- Start  Docker and run : 

    docker pull cramzp/crossquake:tag

3.- Check:

    docker images

4.- Run container of docker with crosstremors

    docker run -it cramzp/crossquake:tag bash

6.- Download last version crosstremors
    git clone https://gitlab.com/cramirezp/crosstremors.git

7.- Run  scrip crossquake

    cd crosstremors

    pipenv shell

    python Main.py

8.- Exit

     exit+enter

Note to run the container again it is only necessary to perform the following steps

1.- docker ps -a

2.- docker start xxxxxxxx(CONTAINER ID)

3.- docker exec -it xxxxxxx(CONTAINER ID) bash

4.- cd crosstremors

    pipenv shell

    python Main.py

5.- Exit 
    exit+enter

After the 1906 San Francisco earthquake (M > 7.9, [1, 2]), H.F. Reid proposed
the Elastic Rebound Theory to explain earthquakes [3]. The Elastic Rebound
Theory basically states that elastic strain is accumulated over a geological fault
locked by friction. When the strain reaches certain treshold, all the accumu-
lated energy is suddenly released and a part of this energy is radiated away as
seismic waves
import obspy
from obspy.core.util import NamedTemporaryFile
from obspy.clients.fdsn import Client as FDSN_Client
from obspy.clients.iris import Client as OldIris_Client
#from ScrolledText import *
from peticion_web import *
from OpenFile_multiple import *
import decimal
import random
import math
from Guardar_datos import *
#from tkFileDialog import askopenfilename
from Lectura_file import *
from Rutas import *
from Pos_Analisis_Resultados import *
from ReName import *
import os
from shutil import rmtree

from Dia_juliano import *

class Descargar_web:
	def abrirArchivoStation(self):

		
		path_sta=self.path_sta
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		print (self.estaciones)

		#archivo =self.pathfileDF()
		#self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF
	def trabajar(self):
		


		self.abrirArchivoStation()
		
		inicia_peticiones=peticion_web()
		inicio_dia_j=Dia_juliano()

		



		self.year_an = str(self.year)
		self.Channel=str(self.Channel)
		self.day=str(self.day)
		self.month=str(self.month)
		
		if len(self.day)<2:
			self.day='0'+str(self.day)
		else:
			self.day=str(self.day)

		if len(self.month)<2:
			self.month='0'+str(self.month)
		else:
			self.month=str(self.month)



		## se incia la configuración nueva
		#------------------------------------------
		if not self.year % 4 and (self.year % 100 or  not self.year % 400):
			bisi=True
		else:
			bisi=False

		meses=[31, 28, 31,30, 31, 30, 31, 31, 30, 31, 30, 31]
		month=int(self.month)
		if bisi == True:
			meses[1]=meses[1]+1
		dias_j=[]
		dias=[]
		#============================ Se indica path para guardar resultados ===============================
		inicia=Guardar_datos()


		aux_d=meses[month-1]+1
		for d in range(int(self.day),aux_d):

			#day_j=inicio_dia_j.main(self.year_an, self.month,self.day)
			day_j=inicio_dia_j.main(self.year_an, self.month,d)
			print ("Julian day", day_j)
			day_j=str(day_j)
			while len(day_j)<3:
				day_j='0'+day_j
			print ('==============================================')
			print(dias_j)
			print ('==============================================')
			dias_analisis=[]

			#

		
		
			#ruta_guardar_datos_end=inicia.guardar_directorio(Tipo)
			ax_pat=os.getcwd()
			ruta_guardar_datos_end=ax_pat+"/"+self.code+"/"+day_j
			#ruta_guardar_datos_end="/media/carlos/MEMORIA"+"/XF/"+day_j
			print (ruta_guardar_datos_end)



			if not os.path.exists(ruta_guardar_datos_end):
				os.makedirs(ruta_guardar_datos_end)




			ruta_guardar_datos=ruta_guardar_datos_end
			print ('==============================================')
			print ( " path for save files", ruta_guardar_datos)
			#datos_rep[4]=ruta_guardar_datos
			estaciones=self.estaciones
			self.ruta_guardar_rep_datos=ruta_guardar_datos
			if not os.path.exists(self.ruta_guardar_rep_datos):
				os.makedirs(self.ruta_guardar_rep_datos)
		


		
			for sta in range(len(self.estaciones)):
				#sta=3
				#self.pathArchivoDE=[]
				aux_rep=[]
				print (self.year_an, self.day, self.Channel, self.estaciones[sta], self.month, self.code)
				datos_rep=[self.month, self.day,self.code, self.estaciones[sta],""]
				self.day=str(d)
				st_z=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], self.Channel)
				#st_e=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHE')
				#st_n=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHN')
				#aux_rep.append(st_z)
				name_rep_z=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+str(self.year_an)+'.'+str(day_j)+'..SAC'
				#name_rep_e=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHE.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
				#name_rep_n=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHN.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
				#print ("name_rep", name_rep)
				##aux_rep.append(name_rep)
				#if st_z!= None and st_e != None and st_n != None:
				if st_z!= None:
					st_z.write(name_rep_z, format='SAC')
					name_rep_z=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+str(self.year_an)+'.'+str(day_j)+'.03.SAC'
					name_rep_n=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+str(self.year_an)+'.'+str(day_j)+'.02.SAC'
					name_rep_e=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+str(self.year_an)+'.'+str(day_j)+'.01.SAC'
					name_new_z=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..'+self.Channel+'E.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
					name_new_n=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..'+self.Channel+'N.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
					name_new_e=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..'+self.Channel+'Z.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
					os.rename(name_rep_z, name_new_z)
					os.rename(name_rep_n, name_new_n)
					os.rename(name_rep_e, name_new_e)


					#st_e.write(name_rep_e, format='SAC') 
					#st_n.write(name_rep_n, format='SAC')
		
		
			if not os.path.exists(ruta_guardar_datos):
				os.makedirs(ruta_guardar_datos)


	def main(self):
		valor = "" #para el inicio los entry tenga b


		print("Input of data for download web")
		self.year=input("Input year of analisys (YYYY):  ")
		try:
			self.year=int(self.year)
			if (self.year)>=1954:
				print("year is:  ", self.year)
				status_year=True
		except Exception as e:
			print("format incorrect")
			status_year=False
		

		
		while status_year == False:
			self.year=input("Input year of analisys (YYYY):  ")
			try:
				self.year=int(self.year)
				if (self.year)>=1954:
					print("year is:  ", self.year)
					status_year=True
			except Exception as e:
				print("format incorrect")
				status_year=False



		self.code=input("Input FDSN code:  ")
		try:
			print("FDSN code is: ", self.code)
			self.code = self.code.upper()
			status_code=True
		except Exception as e:
			print("error input FDSN code")
			status_code=False

		while status_code == False:
			self.code=input("Input FDSN code:  ")
			try:
				print("FDSN code is: ", self.code)
				self.code = self.code.upper()
				status_code=True
			except Exception as e:
				print("error input FDSN code")
				status_code=False
			
			

		self.month=input("Input initial month (mm):  ")
		try:
			self.month=int(self.month)
			if self.month >=1 and self.month<=12:
				print("Initial month is: ", self.month)
				status_month=True
			else:
				print("month incorrect")
				status_month==False
		except Exception as e:
			print("format incorrect")
			status_month=False

		while status_month== False:
			self.month=input("Input initial month:  ")
			try:
				self.month=int(self.month)
				if self.month >=1 and self.month<=12:
					print("Initial month is: ", self.month)
					status_month=True
				else:
					print("month incorrect")
					status_month==False
			except Exception as e:
				print("format incorrect")
				status_month=False



		self.day=input("Input initial day (dd)")
		if not self.year % 4 and (self.year % 100 or  not self.year % 400):
			bisi=True
		else:
			bisi=False
		meses=[31, 28, 31,30, 31, 30, 31, 31, 30, 31, 30, 31]
		month=self.month
		if bisi == True:
			meses[1]=meses[1]+1

		self.day_mont=meses[month-1]
		try:
			self.day=int(self.day)

			
			

			if self.day>=1 or self.day<=self.day_mont:
				status_day=True
		except Exception as e:
			print("Error, format incorrect")
			self.day_mont=meses[month-1]
			status_day=False


		while status_day==False:
			print("day incorrect, please input the day of month: ", self.month, "in the range(1-",self.day_mont,")")
			self.day=input("Input initial day (dd)")
			try:
				self.day=int(self.day)

				if self.day>=1 or self.day<=self.day_mont:
					status_day=True
			except Exception as e:
				print("Error, format incorrect")
				self.day_mont=meses[month-1]
				status_day=False

		print("only enter the first two characters (e.g. HH, HB, LL..)")
		self.Channel=input("Input name Channel:\t  ")
		#print("only enter the first two characters (e.g. HH, HB, LL..)")
		try:
			print("Name Channel is:  ", self.Channel)
			self.Channel = self.Channel.upper()
			status_channel=True
		except Exception as e:
			print("error input name Channel")
			status_channel=False

		while status_channel == False:
			print("only enter the first two characters (e.g. HH, HB, LL..)")
			self.Channel=input("Input name Channel:\t  ")
			
			try:
				print("Name Channel is: ", self.Channel)
				self.Channel = self.Channel.upper()+"?"
				status_code=True
			except Exception as e:
				print("error input name Channel")
				status_code=False



		self.path_sta=input("Input path file stations: \n")

		

		try:
			if (os.path.isfile(self.path_sta)) :
				status_sta=True
		except Exception as e:
			print("Error, format incorrect")
			status_sta=False

		while status_sta== False:
			self.path_sta=input("Input path file stations \n")

		

			try:
				if (os.path.isfile(self.path_sta)) :
					status_sta=True
			except Exception as e:
				print("Error, format incorrect")
				status_sta=False
		
		

		self.mostrarInfo()

	def mostrarInfo(self):
		os.system("clear")
		print ("information input")
		print("year is:\n ", self.year)
		print("FDSN code is:\n ", self.code)
		print("month is:\n ", self.month)
		print("day is:\n ", self.day)
		print("Channel is:\n ", self.Channel)
		print("\n *******************************")
		resp=input("right information? (Y/N)")

		if resp == "Y" or resp =="y" or resp== "yes":
			self.trabajar()
		else:
			print("1.-input information")
			print("2.-cancelar")
			choice=input("Select choice (1-2):\n")

			try:
			
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=3:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 2\n")

			except:
				os.system("clear")
				choice=0
				print("format incorrect, please enter number between 1 and 2\n")


			while choice <=0 or choice>=3:

			
				print("1.-input information")
				print("2.-cancelar")
				choice=input("Select choice (1-2):\n")
				try:
				
					choice=int(choice)
					print ("choice select: ",choice)
					if choice <=0 or choice>=3:
						os.system("clear")
						print("Choice incorrect, please select choice between 1 and 2\n")
				except:
					os.system("clear")
					choice=0
					print("Choice incorrect, please select choice between 1 and 2\n")

			if choice ==1:
				self.main()
			else:
				self.cancelar()
		
	def cancelar(self):
		os.system("clear")		

#inicio=Descargar_web()
#inicio.main()
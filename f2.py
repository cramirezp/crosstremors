import numpy as np
from obspy import read
import matplotlib.pyplot as plt
import math
import os
import os.path
from obspy.imaging.cm import viridis
from obspy.signal.invsim import cosine_taper
from obspy.signal.util import util_geo_km


class FrecuenciaEsquina():

	#def Frecuencia(self, path):
	def Frecuencia(self):
				
		
		# Leer un archivo sismográfico (reemplaza "tus_datos.mseed" con el nombre de tu archivo)
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.10.21/2006.05.10.21.MAZE.HHZ.SAC")
		#st = read('/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.ALPI.ZA.HHZ.SAC')
		
		#st=read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.09.21.12/2006.09.21.12.SNID.HHZ.SAC")
		#st = read('/home/carlos/Documentos/CrossQuake-Data/tremores/2006.03.06.22/2006.03.06.22.MORA.HHZ.SAC')
		#st = read("/home/carlos/Documentos/CrossQuake-Data/2006.062.22/2006.062.22.08.02.SCRI.ZA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.04.17/2006.05.04.17.MORA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.12.15.10/2006.12.15.10.SINN.HHZ.SAC")
		#st=read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.11.28.16/2006.11.28.16.SANM.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.10.18/2006.07.10.18.ESPN.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.14.12/2006.05.14.12.COMA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.04.12.15/2006.04.12.15.JANU.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.10.19/2006.05.10.19.GARC.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.03.06.08/2006.03.06.08.MA02.XF.BHZ.SAC")
		st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.03.06.08/2006.03.06.08.MA04.XF.BHZ.SAC")
		valores=[]
		#st = read(path)
		# Seleccionar una traza del conjunto de datos
		trace = st[0]

		# Parámetros para el cálculo de la frecuencia de esquina
		t_start = 0  # tiempo de inicio en segundos
		t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
		npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
		dt = 1 / trace.stats.sampling_rate

		# Calcular la transformada de Fourier
		freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
		positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
		
		# Tomar solo la parte positiva de la transformada
		freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

		# Calcular la densidad espectral de potencia
		power_spectrum = np.abs(ampl_pos) ** 2

		#plt.loglog(power_spectrum)
		#plt.show()


		# Establecer un límite inferior en la frecuencia para graficar
		lower_freq_limit = 1.0  # Ajusta según tus necesidades

		# Suavizar el power_spectrum con un filtro de media móvil
		window_size = 150  # Tamaño de la ventana del filtro de media móvil
		power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')



		# Identificar la frecuencia máxima y mínima
		max_frequency_index = np.argmax(power_spectrum_smooth)
		min_frequency_index = np.argmin(power_spectrum_smooth)

		# Convertir los índices a frecuencias en Hz
		max_frequency = freq_pos[max_frequency_index]
		min_frequency = freq_pos[min_frequency_index]



		for i in range(len(freq_pos)):
			if freq_pos[i]>= lower_freq_limit:
				indice=i
				break

		#print(indice)
		freq_pos=freq_pos[indice:len(freq_pos)]
		power_spectrum_smooth=power_spectrum_smooth[indice:len(power_spectrum_smooth)]



		# Crear listas para almacenar los valores de frecuencia y densidad espectral
		frequencies = []
		power_values = []

		# Iterar sobre los valores y almacenarlos en las listas
		for freq_value, power_value in zip(freq_pos, power_spectrum_smooth):
			frequencies.append(freq_value)
			power_values.append(power_value)

		# Convertir las listas a arrays de NumPy
		frequencies = np.array(frequencies)
		power_values = np.array(power_values)


		# Filtrar las frecuencias y el espectro de potencia suavizado según el límite inferior
		filtered_freq_pos = freq_pos[freq_pos >= lower_freq_limit]
		filtered_power_spectrum_smooth = power_spectrum_smooth[freq_pos >= lower_freq_limit]

		# Convertir las unidades del eje y a "medias dinas * cm"
		power_in_mdc = filtered_power_spectrum_smooth * 1e-2  # 1 (unidad estándar) = 1 mediana dina * cm

		# Almacenar los valores en listas
		frequencies = list(filtered_freq_pos)
		power_values = list(power_in_mdc)



		# Aplicar logaritmo a los arrays
		#log_frequencies = np.log(frequencies)
		#log_power_values = np.log(power_values)


		log_power_values=power_values
		log_frequencies=frequencies

		# Encontrar la frecuencia de esquina (índice del 90% de la potencia acumulada)
		cumulative_power = np.cumsum(power_spectrum) / np.sum(power_spectrum)
		corner_frequency_index = np.argmax(cumulative_power >= 0.8)


		# Asegurarse de que el índice esté dentro de los límites
		if corner_frequency_index < len(filtered_freq_pos):
			corner_frequency = filtered_freq_pos[corner_frequency_index]
			print(f"Frecuencia de esquina: {corner_frequency}")
		else:
			print("Índice de frecuencia de esquina fuera de los límites.")

		# Convertir el índice a frecuencia en Hz
		corner_frequency = freq_pos[corner_frequency_index]

		st_crece=0
		st_decre=0
		contc=0
		contd=0



		for i in range(len(log_power_values)):
			if log_power_values[i]<= log_power_values[i+1]:

				
				if st_crece==1 and st_decre==1 and contd>150:
			
					punto_int= log_power_values[i]
					break
				else:
					st_crece=1
					contc+=1
					#print(log_power_values[i], "-- Crece" )
			else:
				st_decre=1
				contd+=1
				#print(log_power_values[i], "-- Decrece" )

				st_crece=0


		#print("punto_int ", punto_int)



		puntos_int=[]
		indice_int=[]
		fin=len(log_frequencies)
		#fin=log_frequencies[fin]
		x_aux=np.arange(0, fin, 0.0001)

		for i in range(len(log_power_values)):
				if round(log_power_values[i],2) == round(punto_int, 2):
					puntos_int.append(log_power_values[i])
					indice_int.append(i)	
		#print(puntos_int)
		#print(indice_int, "indices")
		
		x11=0
		y11=0
		x01=0
		y01=0
		am=0
		for i in (indice_int):
			#print(log_frequencies[i])
			#plt.scatter(log_frequencies[i], log_power_values[i], color='red', label='Punto (1, 2)')
			if x01==0:
				if am==0:
					Mo=log_frequencies[i-100]
					am=1
				x01=log_frequencies[i]
				y01=log_power_values[i]
				xx0=1
			x11=log_frequencies[i]
			y11=log_power_values[i]
			x0=i

		#print("x11: ", x11, " y11: ", y11)
		fin=len(log_frequencies)
		#print("valor final  ", x0)
		#print(len(log_frequencies))
		x00=0
		x=0
		y2=0
		nn=len(log_frequencies)
		for j in range(0, nn, 10):
			#print(j)
			#j=2400
			cont=y11-1
			if j>= x0:
				#print("entra al if***")
				x1=round(log_frequencies[j], 2)
				#x1=round(log_frequencies[nn-1], 2)
				y1=round(punto_int, 2)
				# Calcular la pendiente
				m = -1

				# Calcular el término independiente (b) usando la ecuación de la recta
				b = y1 - m * x1
		
				# Crear un conjunto de puntos para la recta
				x = np.linspace(cont, log_frequencies[nn-1], 55)
				y2 = m * x + b
				status_cruce=False
		
				n=len(y2)
				for jj in range(n):
			
					if status_cruce==False:
						for ji in range(len(log_power_values)):
							if log_power_values[ji]<punto_int and y2[jj]<punto_int:
								if round(y2[jj],4) == round(log_power_values[ji],4) :
									#print("con j= ", j, " se encuentran en la recta")
									status_cruce=True
									break
						
						if status_cruce == True:
							cont=cont+0.1
							break
				if status_cruce == False:
					x00=j
					break


		xi=log_frequencies[x00]
		yi=punto_int
		#print(xi, " ------ ", yi)

		#print("range is : ", log_frequencies[xx0], " ------ ", log_frequencies[x0])
		if corner_frequency <10:
			xi=round(corner_frequency, 2)
			print('frecuencia de esquina: ', round(corner_frequency, 2))
		else:
			xi=round(xi, 2)
			print("Frecuencia de esquina: ", xi)


		corner_frequency=xi
		# Calcular el momento sísmico
		momento = self.calcular_momento_sismico(trace)
		valores.append(xi)
		Mo=round(Mo, 2)
		valores.append(momento)
		#return valores




		x1=corner_frequency
		
		y1= punto_int


		print(x1,  y1)

		x=35
		y=100

		# # Ángulo en grados
		# theta_deg = 135

		# # Convertir el ángulo a radianes
		# theta_rad = np.radians(theta_deg)

		# # Calcular la pendiente
		# m = np.tan(theta_rad)

		# # Crear datos para la línea
		# #x = np.linspace(6, 12, 10)
		# #y = m * (x - x1) + y1




		# Graficar en escala logarítmica con unidades modificadas en el eje y
		plt.loglog(frequencies, power_values, color='black', linestyle='--')

		# Configurar el formato del eje y con unidades modificadas
		plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: '{:.0e}'.format(x)))


		# #Graficas
		#plt.loglog(log_frequencies[freq_pos >= lower_freq_limit], log_power_values[freq_pos >= lower_freq_limit], color='black', linestyle='--')
		# Configurar el formato del eje x con unidades modificadas
		#plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: '{:.0e}'.format(y)))

		#plt.ylabel("Frecuencia (medias dinas * cm)")
		# #plt.plot(log_frequencies, log_power_values)
		# #plt.xscale('log')  # Establecer el eje x en escala logarítmica
		# #plt.yscale('log')  # Establecer el eje y en escala logarítmica
		# #plt.axvline(x=xi, color='red', linestyle='--', label=f'Frecuencia de Esquina: {xi:.2f} Hz')
		# #plt.plot(x,y2)
		# #plt.loglog(x, y, label='Línea a 135°')
		plt.plot([x1, x], [y1, y], color='black', linestyle='--')
		# #plt.axvline(x=corner_frequency, color='black', linestyle='--')
		# #plt.scatter(xi, yi, color='red', label=f'corner frecuency: {xi:.2f} Hz')
		plt.scatter(xi, yi, color='black', label=f'corner frecuency: {corner_frequency:.2f} Hz')
		# plt.scatter(Mo, yi, color='black', label=f'seismic moment: {Mo:.2f} Hz')
		# #plt.axvline(x=Mo, color='green', linestyle='--')
		plt.axhline(y=punto_int, color='black', linestyle='--')
		plt.legend()
		plt.show()
	def calcular_momento_sismico(self, tr):
		# Obtener la traza de forma de onda sísmica
		data = tr.data
    
		# Obtener la información sobre la traza
		dt = tr.stats.delta
		rho = 2670  # Densidad típica de la corteza terrestre en kg/m^3
		vs = 3500   # Velocidad de corte típica en m/s
		A = 1e6     # Área típica del fallo en m^2
		sigma = 10e6  # Resistencia a la fractura típica en N/m^2
    
		# Calcular el momento sísmico
		momento_sismico = np.sum(data ** 2) * dt * rho * vs**2 * A * sigma
    
		return momento_sismico


	def Main(self, estaciones):
		#for e in range(len(responses)):
			#print(responses[e])
		file="/home/carlos/Documentos/CrossQuake-Data/tremores/"
		file22="/home/carlos/Documentos/CrossQuake-Data/"
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		self.path=[]
		path11=[]
		self.promedios=[]
		self.promedios1=[]
		for i in archivos:
			ruta0=	file+i
			self.path.append(ruta0)
			path11.append(i)

		ruta_guardar_datos=file+'corner_frequency_comple'
		if not os.path.exists(ruta_guardar_datos):
			os.makedirs(ruta_guardar_datos)

		ruta00=''
		for i in range(len(self.path)):
			print (self.path[i])
			ruta00=self.path[i]
			fo = open(ruta_guardar_datos+'/'+path11[i]+'.txt', 'a')
			arch = os.listdir(self.path[i])
			arch=sorted(arch)
			remove_esta=[]
			cont=0
			suma=0
			suma1=0

			for archivo in arch:
				if archivo.rfind('HHZ') != -1 or archivo.rfind('BHZ') != -1:
					print(archivo)
					for estacion in range(len(estaciones)):
						if archivo.rfind(estaciones[estacion]) != -1:
							esta=estaciones[estacion]
							break
					path=(ruta00+'/'+archivo)
					freq_v= self.Frecuencia(path)
					freq=freq_v[0]
					Mo=freq_v[1]

					

					cont=cont+1
					suma= suma+ freq
					suma1=suma1+Mo
					prom=round((suma/cont), 2)
					promMo=round((suma1/cont), 2)
					self.promedios.append(round(prom,1))
					self.promedios1.append(round(promMo, 1))
					fo.write(esta+'\t'+ str(freq)+'\t'+str(Mo)+'\n')
			fo.write('average freq:\t'+str(prom)+'\n')
			fo.write('average Mo:\t'+str(promMo))
			fo.close()
			print('\n')
			ruta00=''


		# Crear un diccionario para almacenar las cuentas
		conteo_valores = {}

		# Contar la frecuencia de cada valor en la lista
		for valor in self.promedios:
			if valor in conteo_valores:
				conteo_valores[valor] += 1
			else:
				conteo_valores[valor] = 1

		# Guardar los resultados en un archivo de texto
		with open(file+"HistogramaFreq.txt", "w") as archivo:
			for valor, cantidad in conteo_valores.items():
				archivo.write(f"{valor}: {cantidad} \n")

		# Crear un diccionario para almacenar las cuentas
		conteo_valores = {}

		# Contar la frecuencia de cada valor en la lista
		for valor in self.promedios1:
			if valor in conteo_valores:
				conteo_valores[valor] += 1
			else:
				conteo_valores[valor] = 1

		# Guardar los resultados en un archivo de texto
		with open(file+"HistogramaMo.txt", "w") as archivo:
			for valor, cantidad in conteo_valores.items():
				archivo.write(f"{valor}: {cantidad} \n")




















		# #for i in range(len(path)):
		# 
		# if not os.path.exists(ruta_guardar_datos):
		# 		os.makedirs(ruta_guardar_datos)
		# for i in range(len(path)):
		# 	#print("*******    ", path[i])
		# 	fo = open(ruta_guardar_datos+'/'+path11[i]+'.txt', 'a')
		# 	ruta=path[i]
		# 	print(ruta)
		# 	if os.path.isdir(ruta):
		# 		arch = os.listdir(ruta)
		# 		remove_esta=[]
		# 		cont=0
		# 		suma=0
		# 		for archivo in arch:
		# 			#print('archivo', archivo)
		# 			if archivo.rfind('HHZ') != -1:
		# 				#print(archivo)
		# 				for estacion in range(len(estaciones)):
		# 					if archivo.rfind(estaciones[estacion]) != -1:
		# 						esta=estaciones[estacion]
		# 						break
		# 				remove_esta.append(archivo)
		# 				#print(ruta)
		# 				path=(ruta+'/'+archivo)
		# 				#print('******** ', path)
		# 				freq= self.Frecuencia(path)
		# 				cont=cont+1
		# 				suma= suma+ freq
					
		# 				fo.write(esta+'\t'+ str(freq)+'\n')
		# 		prom=round((suma/cont), 2)
		# 		promedios.append(prom)
		# 		fo.write('prom:\t'+str(prom))
		# 		#print('\n')

	
	

		# # Crear un diccionario para almacenar las cuentas
		# conteo_valores = {}

		# # Contar la frecuencia de cada valor en la lista
		# for valor in promedios:
		# 	if valor in conteo_valores:
		# 		conteo_valores[valor] += 1
		# 	else:
		# 		conteo_valores[valor] = 1

		# # Guardar los resultados en un archivo de texto
		# with open(file22+"Histograma.txt", "w") as archivo:
		# 	for valor, cantidad in conteo_valores.items():
		# 		archivo.write(f"{valor}: {cantidad} veces\n")



							
						

			
				

inicio = FrecuenciaEsquina()
estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
for i in range(56):

	if i<9:
		estaciones.append('MA0'+str(i+1))
	else:
		estaciones.append('MA'+str(i+1))

#inicio.Main(estaciones)
inicio.Frecuencia()
from Fourierft import *
import math
import obspy

class Correlacion():
	def trabaja(self, parte, senal02, ex):


		fourier=Fourierft()
		f0=fourier.transformada(parte, ex)
		f1=fourier.transformada(senal02, ex)

		c=[]

		
		for i in range(len(f0)):
			c.append(f1[i].conjugate()*f0[i])

		#====================== se realiza la transformada inversa para localizar el punto maximo de correlacion================
		R_xy = fourier.inversa(c, ex)

		R_xy=np.asarray(R_xy)
		R_xy=abs(R_xy)


		#====================== se calcula la energia de cada senal================
		L2 = [n2**2 for n2 in senal02]
		n=sum(L2)
	
		L1 = [n1**2 for n1 in parte]
		m=sum(L1)
		#====================== se normaliza la correlacion, valores de 0 a 1 ================
		for i in range(len(R_xy)):
		
			R_xy[i]=R_xy[i]/(n)
		
		R_xy=R_xy.tolist()

		maximo=max(R_xy)
		print(maximo)
		return R_xy



# inicio= Correlacion()
# Parte="/home/carlos/Documentos/prueba.sac"
# senal02="/home/carlos/Documentos/CrossQuake-Data/catalogo_tremores2/2006.074.08.15/2006074_MA05_08_15.BHZ.SAC"
# st1=obspy.read(Parte)

# parte=st1[0].data


# st2=obspy.read(senal02)
		
# senal02=st2[0].data


# ex=math.log(len(senal02),2)
# inicio.trabaja(parte, senal02, ex)
import obspy
import numpy as np
import matplotlib.pyplot as plt
import os
from Lectura_file import *
class Graf_HV():
    def abrirArchivoStation(self):

        
        path_sta=self.path_sta
        inicio=Lectura_file()
        #path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
        a=inicio.main(path_sta)
        aux=[]
        for i in range(len(a)):
            if i!=0:
                aux.append(a[i][0])
        self.estaciones=aux
        print (self.estaciones)

        #archivo =self.pathfileDF()
        #self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
        #self.pathArchivoSta=archivo
        #print  self.pathArchivoDF
    def calcular_espectro(self, trace, delta):

        #f, Pxx = plt.psd(trace.data, NFFT=1024, Fs=1.0 / delta, color='black', linestyle='--')

        t_start = 0  # tiempo de inicio en segundos
        t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
        npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
        #npts = 26000
        #print("npts-------", npts)
        dt = 1 / trace.stats.sampling_rate
        #print("dt-------", dt)
        #print("t_end-------", t_end)
        #plt.semilogx(f, Pxx, color='black', linestyle='--')
        # Calcular la transformada de Fourier
        freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
        positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
        
        # Tomar solo la parte positiva de la transformada
        freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

        # Calcular la densidad espectral de potencia
        power_spectrum = np.abs(ampl_pos) ** 2

        #plt.loglog(power_spectrum)
        #plt.show()self.


        # Establecer un límite inferior en la frecuencia para graficar
        lower_freq_limit = 1.0  # Ajusta según tus necesidades

        # Suavizar el power_spectrum con un filtro de media móvil
        window_size = 150  # Tamaño de la ventana del filtro de media móvil
        power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')

        return freq_pos,  power_spectrum_smooth


    def calcular_relacion_hv(self, traza_n, traza_e, traza_z):
        # Calcular los espectros de amplitud para las componentes horizontales y verticales
        f_n, A_n = self.calcular_espectro(traza_n, traza_n.stats.delta)
        f_e, A_e = self.calcular_espectro(traza_e, traza_e.stats.delta)
        f_z, A_z = self.calcular_espectro(traza_z, traza_z.stats.delta)
    
        # Calcular la relación H/V
        #relacion_hv = np.sqrt((A_n**2 + A_e**2) / A_z**2)
        n = [x**2 for x in A_n]
        e = [x**2 for x in A_e]
        z = [x**2 for x in A_z]
        suma=0
        suman=0
        sumae=0
        sumazz=0
        sumaz=0
        b=0
        for i in range(len(A_z)):
            #print(tr_n[i]**2, tr_e[i]**2)
            #print(tr_n[i]**2+tr_e[i]**2)
            a=(A_n[i]**2+A_e[i]**2)
            b=a+b
            suma=suma+((A_n[i]*A_n[i])+(A_e[i]*A_e[i]))
            suman=suman+(A_n[i]**2)
            sumae=sumae+(A_e[i]**2)
            sumazz=sumazz+(A_z[i]**2)
            sumaz=sumaz+(abs(A_z[i]))

        #print(b)
        #print(sumaz, suma)
        #res=np.sqrt(suma/sumaz)
        res=np.sqrt((suman+sumae)/sumazz)
        #print(res, "****")
        suma = np.array(n)+np.array(e)
        hv=suma/np.array(z)
        relacion_hv=np.sqrt(hv)
        #print(relacion_hv)
        return f_n, relacion_hv


    def Proceso(self,estacion1):
        ## Cargar las formas de onda de las tres componentes (N, E, Z)
        #archivo_sac_n = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHN.SAC"
        #archivo_sac_e = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHE.SAC"
        #archivo_sac_z = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHZ.SAC"
        #file="/home/carlos/Documentos/CrossQuake-Data/tremores/"
        file=self.pathCat+'/'
        #file22="/home/carlos/Documentos/CrossQuake-Data/"
        file22=os.getcwd()
        print(estacion1)
        archivos=os.listdir(file)
        archivos=sorted(archivos)
        self.path=[]
        path11=[]
        self.promedios=[]
        self.promedios1=[]
        self.f=[]
        self.relacion=[]
        #cont_sta=np.zeros(len(estaciones))
        #sitio_sta=np.zeros(len(estaciones))
        #prom_sta=   np.zeros(len(estaciones))

        for i in archivos:
            ruta0=  file+i
            self.path.append(ruta0)
            path11.append(i)

        self.ruta_guardar_datos=file22+ '/VH_'+self.identificador
        if not os.path.exists(self.ruta_guardar_datos):
            os.makedirs(self.ruta_guardar_datos)
        ruta00=''
        for i in range(len(self.path)):
            #print (self.path[i])
            ruta00=self.path[i]
            #fo = open(ruta_guardar_datos+'/'+path11[i]+'.txt', 'a')
            if os.path.isdir(ruta00):
                arch = os.listdir(self.path[i])
                arch=sorted(arch)
                remove_esta=[]
                cont=0
                suma=0
                suma1=0

                #for estacion1 in range(len(estaciones)):
                    
                vec=[]
                #estacion1='ALPI'
                self.estaciona=estacion1
                for archivo in arch:
                    

                    
                    if archivo.rfind(estacion1) != -1 :
                        esta=estacion1
                            
                        path=(ruta00+'/'+archivo)
                        vec.append(path)
                if len(vec)!=0:
                    relacion=self.estaciones_analisis(vec, estacion1)
                    #self.graficar()                               

        #def graficar(self):
        # traza_n = obspy.read(archivo_sac_n)[0]
        # traza_e = obspy.read(archivo_sac_e)[0]
        # traza_z = obspy.read(archivo_sac_z)[0]
        if len(self.relacion)>0:    
            # # Calcular la relación H/V
            # f_hv, relacion_hv = self.calcular_relacion_hv(traza_n, traza_e, traza_z)
            #print(" size relacion", len(self.relacion))
            prom_hv=   np.zeros(len(self.relacion))
            #print(len(self.relacion), "+++++++++++++")
        

            # Obtener la lista con menos elementos usando la función len
            min_lista = min(self.relacion, key=len)

            # Obtener la posición de la lista con menos elementos
            posicion = self.relacion.index(min_lista)
            #print(f"La lista con menos elementos está en la posición {posicion} y tiene {len(min_lista)} elementos.")


            # Obtener la lista con mayor elementos usando la función len
            max_lista = max(self.relacion, key=len)

            # Obtener la posición de la lista con mayor elementos
            posicionm = self.relacion.index(max_lista)
            #print(f"La lista con mayor elementos está en la posición {posicionm} y tiene {len(max_lista)} elementos.")
            for i in range(len(self.relacion)):
                if len(self.relacion[i]) > len(min_lista) :
                    
                    self.relacion[i]=self.relacion[i][400:len(self.relacion[i])-7250]
                    self.f[i]=self.f[i][400:len(self.f[i])-7250]
                #print(len(self.relacion[i]))


            # Obtener la lista con mayor elementos usando la función len
            max_lista = max(self.relacion, key=len)

            # Obtener la posición de la lista con mayor elementos
            posicionm = self.relacion.index(max_lista)
            #print(f"La lista con mayor elementos está en la posición {posicionm} y tiene {len(max_lista)} elementos.")
            for i in range(len(self.relacion)):
                if len(self.relacion[i]) < len(max_lista):
                    faltantes=len(max_lista)-len(self.relacion[i])
                    ultimo_valor = self.relacion[i][-1]
                    # Agregar tres veces el último valor al final del vector
                    vector_actualizado = np.append(self.relacion[i], [ultimo_valor] * faltantes)
                    self.relacion[i]=vector_actualizado
                #print(len(self.relacion[i]))


            self.elementos=[]
            self.fre=[]

            for i in range(len(self.relacion)):
                valor_maximo = np.max(self.relacion[i])
                if valor_maximo <20:
                    self.elementos.append(self.relacion[i])
                    #self.fre.append(self.f[i])


            self.fre=self.f[posicionm]

            if len(self.elementos)<1:
                for i in range(len(self.relacion)):
                    valor_maximo = np.max(self.relacion[i])
                    if valor_maximo <100:
                        self.elementos.append(self.relacion[i])

            
            if len(self.elementos)<1:
                for i in range(len(self.relacion)):
                    valor_maximo = np.max(self.relacion[i])
                    #if valor_maximo <1000:
                    self.elementos.append(self.relacion[i])          
            n=len(self.elementos)
            #print(len(self.elementos))
            #print("*+*+*+*+*", len(self.elementos[0]))
            self.promrelacion=np.zeros(len(self.elementos[0]))
            sumaprom=np.zeros(len(self.elementos[0]))
            
            for i in range(len(self.elementos)):
            
                for j in range(len(self.elementos[i])):
                    sumaprom[j]=sumaprom[j]+self.elementos[i][j]

            for i in range(len(sumaprom)):
                self.promrelacion[i]=sumaprom[i]/n

        
            posicion_maximo = np.argmax(self.promrelacion)
            fo = open(self.ruta_guardar_datos+'/'+'Ratio_VH.txt', 'a')
            fo.write(self.estaciona+'\t\t'+str(round(self.fre[posicion_maximo], 1))+'\n')
            print("valor maximo es ", self.fre[posicion_maximo])
            # Graficar la relación H/V
            fig, ax = plt.subplots()
            #plt.figure(figsize=(10, 6))
            for i in range(len(self.elementos)):
                ax.semilogx(self.fre, self.elementos[i], color='gray')
            ax.semilogx(self.fre, self.promrelacion, color='black', label=self.estaciona)
            #plt.semilogx(self.f[posicionm], self.relacion[0], color='black', linestyle='--', label='ZAPO')
            ax.set_xlabel('Period. T(s)')
            ax.set_ylabel('H/V Response Spectral Ratio')
            ax.legend()
            ax.grid(True, which='both', linestyle='--', linewidth=0.5)
            # Guardar la figura como una imagen (por ejemplo, en formato PNG)
            fig.savefig(self.ruta_guardar_datos+'/'+self.estaciona+'.png')
            #plt.show()
    def estaciones_analisis(self, vec, estacion):
        #print("estacion analizado: ", estacion)
        archivo_sac_e=''
        archivo_sac_n=''
        archivo_sac_z=''
        if len(vec)==3:
            for i in range(len(vec)):
                #print(i)
                # Cargar las formas de onda de las tres componentes (N, E, Z)
                archivo_sac_e = vec[0]
                archivo_sac_n = vec[1]
                archivo_sac_z = vec[2]
                # # Cargar las formas de onda de las tres componentes (N, E, Z)
                # archivo_sac_n = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHN.SAC"
                # archivo_sac_e = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHE.SAC"
                # archivo_sac_z = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHZ.SAC"
            #print("archivo_sac_e", archivo_sac_e)
            #print("archivo_sac_n", archivo_sac_n)
            #print("archivo_sac_z", archivo_sac_z)
        if archivo_sac_e!='':
            traza_e = obspy.read(archivo_sac_e)[0]
            traza_z = obspy.read(archivo_sac_z)[0]
            traza_n = obspy.read(archivo_sac_n)[0]
            #print(len(traza_z))
            # if len(traza_z)<26001:
            #     faltantes=26001-len(traza_z)
            #     for relleno in range(faltantes):
            #         traza_z.append(0)
            #         traza_e.append(0)
            #         traza_n.append(0)

            # Calcular la relación H/V
            f_hv, relacion_hv = self.calcular_relacion_hv(traza_n, traza_e, traza_z)
            self.f.append(f_hv)
            self.relacion.append(relacion_hv)

    def trabajar(self):
        self.abrirArchivoStation()
        #self.estaciones
        for i in self.estaciones:
            self.Proceso(i)
    def main(self):
        valor = "" #para el inicio los entry tenga b


        print("Input of data for ratio H/V")


        self.identificador=input("Input name identificador for corner frecuency: \n")
        archivo=input("Input path catalog (tremors)")
        

        try:
            if (os.path.isdir(archivo)) :
                self.pathCat=archivo
                status_df=True
        except Exception as e:
            print("Error, format incorrect")
            status_df=False
        while status_df==False:
            print("path not is a dir")
            archivo=input("Input path catalog (tremors)")
        

            try:
                if (os.path.isdir(archivo)) :
                    self.pathCat=archivo
                    status_df=True
            except Exception as e:
                print("Error, format incorrect")
                status_df=False


        


        self.path_sta=input("Input path file stations: \n")
        try:
            if (os.path.isfile(self.path_sta)) :
                status_sta=True
        except Exception as e:
            print("Error, format incorrect")
            status_sta=False

        while status_sta== False:
            self.path_sta=input("Input path file stations \n")

        

            try:
                if (os.path.isfile(self.path_sta)) :
                    status_sta=True
            except Exception as e:
                print("Error, format incorrect")
                status_sta=False
        
        
        
        self.mostrarInfo()

    def mostrarInfo(self):
        os.system("clear")
        print ("information input")
        print("Path catalog master is:\n ", self.pathCat)
        print("Path file station is:\n ", self.path_sta)
        print("\n *******************************")
        resp=input("right information? (Y/N)")

        if resp == "Y" or resp =="y" or resp== "yes":
            self.trabajar()
        else:
            print("1.-input information")
            print("2.-cancelar")
            choice=input("Select choice (1-2):\n")

            try:
            
                choice=int(choice)
                print ("choice select: ",choice)
                if choice <=0 or choice>=3:
                    os.system("clear")
                    print("Choice incorrect, please select choice between 1 and 2\n")

            except:
                os.system("clear")
                choice=0
                print("format incorrect, please enter number between 1 and 2\n")


            while choice <=0 or choice>=3:

            
                print("1.-input information")
                print("2.-cancelar")
                choice=input("Select choice (1-2):\n")
                try:
                
                    choice=int(choice)
                    print ("choice select: ",choice)
                    if choice <=0 or choice>=3:
                        os.system("clear")
                        print("Choice incorrect, please select choice between 1 and 2\n")
                except:
                    os.system("clear")
                    choice=0
                    print("Choice incorrect, please select choice between 1 and 2\n")

            if choice ==1:
                self.main()
            else:
                self.cancelar()
        
    def cancelar(self):
        os.system("clear")      

        




# inicio=Graf_HV()
# estaciones=['ALPI', 'BAVA', 'CANO', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
# for i in range(56):

#     if i<9:
#         estaciones.append('MA0'+str(i+1))
#     else:
#         estaciones.append('MA'+str(i+1))

# for i in estaciones:
#     inicio.Main(i)

#inicio.Main("MA09")
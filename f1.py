import numpy as np
from obspy import read
import matplotlib.pyplot as plt
import math

class Esquina():
	"""docstring for Frecuencia"""
	def main(self):
		
		
		# Leer un archivo sismográfico (reemplaza "tus_datos.mseed" con el nombre de tu archivo)
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.10.21/2006.05.10.21.MAZE.HHZ.SAC")
		st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.04.08.21/2006.04.08.21.MORA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.14.12/2006.05.14.12.COMA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.04.12.15/2006.04.12.15.JANU.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.04.17/2006.05.04.17.MORA.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.12.17/2006.05.12.17.OLOT.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.10.19/2006.05.10.19.GARC.HHZ.SAC")
		#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.10.18/2006.07.10.18.ESPN.HHZ.SAC")
		# Seleccionar una traza del conjunto de datos
		trace = st[0]

		# Parámetros para el cálculo de la frecuencia de esquina
		t_start = 0  # tiempo de inicio en segundos
		t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
		npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
		dt = 1 / trace.stats.sampling_rate

		# Calcular la transformada de Fourier
		freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
		positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
		
		# Tomar solo la parte positiva de la transformada
		freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

		# Calcular la densidad espectral de potencia
		power_spectrum = np.abs(ampl_pos) ** 2


		# Establecer un límite inferior en la frecuencia para graficar
		lower_freq_limit = 1.0  # Ajusta según tus necesidades

		# Suavizar el power_spectrum con un filtro de media móvil
		window_size = 400  # Tamaño de la ventana del filtro de media móvil
		power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')



		# Identificar la frecuencia máxima y mínima
		max_frequency_index = np.argmax(power_spectrum_smooth)
		min_frequency_index = np.argmin(power_spectrum_smooth)

		# Convertir los índices a frecuencias en Hz
		max_frequency = freq_pos[max_frequency_index]
		min_frequency = freq_pos[min_frequency_index]



		for i in range(len(freq_pos)):
			if freq_pos[i]>= lower_freq_limit:
				indice=i
				break

		#print(indice)
		freq_pos=freq_pos[indice:len(freq_pos)]
		power_spectrum_smooth=power_spectrum_smooth[indice:len(power_spectrum_smooth)]



		# Crear listas para almacenar los valores de frecuencia y densidad espectral
		frequencies = []
		power_values = []

		# Iterar sobre los valores y almacenarlos en las listas
		for freq_value, power_value in zip(freq_pos, power_spectrum_smooth):
			frequencies.append(freq_value)
			power_values.append(power_value)

		# Convertir las listas a arrays de NumPy
		frequencies = np.array(frequencies)
		power_values = np.array(power_values)

		# Aplicar logaritmo a los arrays
		log_frequencies = np.log(frequencies)
		log_power_values = np.log(power_values)


		#log_power_values=power_values
		#log_frequencies=frequencies

		st_crece=0
		st_decre=0
		contc=0
		contd=0



		for i in range(len(log_power_values)):
			if log_power_values[i]<= log_power_values[i+1]:

				#if st_crece==1 and st_decre==1 and contc>200 and contd>500:
				if st_crece==1 and st_decre==1 and contc>500 and contd>1200:
			
					punto_int= log_power_values[i]
					break
				else:
					st_crece=1
					contc+=1
					#print(log_power_values[i], "-- Crece" )
			else:
				st_decre=1
				contd+=1
				#print(log_power_values[i], "-- Decrece" )

				st_crece=0


		#print("punto_int ", punto_int)



		puntos_int=[]
		indice_int=[]
		fin=len(log_frequencies)
		#fin=log_frequencies[fin]
		x_aux=np.arange(0, fin, 0.0001)

		for i in range(len(log_power_values)):
				if round(log_power_values[i],2) == round(punto_int, 2):
					puntos_int.append(log_power_values[i])
					indice_int.append(i)	
		#print(puntos_int)
		#print(indice_int, "indices")
		x11=0
		y11=0
		x01=0
		y01=0
		for i in (indice_int):
			#print(log_frequencies[i])
			#plt.scatter(log_frequencies[i], log_power_values[i], color='red', label='Punto (1, 2)')
			if x01==0:
				x01=log_frequencies[i]
				y01=log_power_values[i]
				xx0=1
			x11=log_frequencies[i]
			y11=log_power_values[i]
			x0=i

		#print("x11: ", x11, " y11: ", y11)
		fin=len(log_frequencies)
		#print("valor final  ", x0)
		#print(len(log_frequencies))
		x00=0
		x=0
		y2=0
		nn=len(log_frequencies)
		for j in range(0, nn, 10):
			#print(j)
			#j=2400
			cont=y11-1
			if j>= x0:
				#print("entra al if***")
				x1=round(log_frequencies[j], 2)
				#x1=round(log_frequencies[nn-1], 2)
				y1=round(punto_int, 2)
				# Calcular la pendiente
				m = -1

				# Calcular el término independiente (b) usando la ecuación de la recta
				b = y1 - m * x1
		
				# Crear un conjunto de puntos para la recta
				x = np.linspace(cont, log_frequencies[nn-1], 55)
				y2 = m * x + b
				status_cruce=False
		
				n=len(y2)
				for jj in range(n):
			
					if status_cruce==False:
						for ji in range(len(log_power_values)):
							if log_power_values[ji]<punto_int and y2[jj]<punto_int:
								if round(y2[jj],4) == round(log_power_values[ji],4) :
									#print("con j= ", j, " se encuentran en la recta")
									status_cruce=True
									break
						
						if status_cruce == True:
							cont=cont+0.1
							break
				if status_cruce == False:
					x00=j
					break


		xi=log_frequencies[x00]
		yi=punto_int
		print(xi, " ------ ", yi)

		#print("range is : ", log_frequencies[xx0], " ------ ", log_frequencies[x0])

		print("Frecuencia de esquina: ", xi)







		#Graficas
		plt.plot(log_frequencies, log_power_values)
		#plt.xscale('log')  # Establecer el eje x en escala logarítmica
		#plt.yscale('log')  # Establecer el eje y en escala logarítmica
		#plt.axvline(x=xi, color='red', linestyle='--', label=f'Frecuencia de Esquina: {xi:.2f} Hz')
		#plt.plot(x,y2)
		plt.scatter(xi, yi, color='red', label=f'Frecuencia de Esquina: {xi:.2f} Hz')
		plt.scatter(x11, y11, color='black')
		plt.scatter(x01, y01, color='black')
		plt.axhline(y=punto_int, color='blue', linestyle='--')
		plt.legend()
		plt.show()

inicio=Esquina()
inicio.main()
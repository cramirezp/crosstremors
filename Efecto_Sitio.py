import obspy
import numpy as np
import matplotlib.pyplot as plt
import os

class EfectoSitio(object):
    def calcular_espectro(self, trace, delta):


        t_start = 0  # tiempo de inicio en segundos
        t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
        npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
        dt = 1 / trace.stats.sampling_rate

        # Calcular la transformada de Fourier
        freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
        positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
        
        # Tomar solo la parte positiva de la transformada
        freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

        # Calcular la densidad espectral de potencia
        power_spectrum = np.abs(ampl_pos) ** 2

        #plt.loglog(power_spectrum)
        #plt.show()


        # Establecer un límite inferior en la frecuencia para graficar
        lower_freq_limit = 1.0  # Ajusta según tus necesidades

        # Suavizar el power_spectrum con un filtro de media móvil
        window_size = 150  # Tamaño de la ventana del filtro de media móvil
        power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')

        return freq_pos,  power_spectrum_smooth
        
    def calcular_relacion_ne_z(self, traza_n, traza_e, traza_z):
        # Calcular la relación NE/Z
        # Calcular los espectros de amplitud para las componentes horizontales y verticales
        f_n, tr_n = self.calcular_espectro(traza_n, traza_n.stats.delta)
        f_e, tr_e = self.calcular_espectro(traza_e, traza_e.stats.delta)
        f_z, tr_z = self.calcular_espectro(traza_z, traza_z.stats.delta)
        #relacion_ne_z = np.sqrt(tr_n.data**2 + tr_e.data**2) / np.abs(tr_z.data)
        suma=0
        suman=0
        sumae=0
        sumazz=0
        sumaz=0
        b=0
        for i in range(len(tr_z)):
            #print(tr_n[i]**2, tr_e[i]**2)
            #print(tr_n[i]**2+tr_e[i]**2)
            a=(tr_n[i]**2+tr_e[i]**2)
            b=a+b
            suma=suma+((tr_n[i]*tr_n[i])+(tr_e[i]*tr_e[i]))
            suman=suman+(tr_n[i]**2)
            sumae=sumae+(tr_e[i]**2)
            sumazz=sumazz+(tr_z[i]**2)
            sumaz=sumaz+(abs(tr_z[i]))

        #print(b)
        #print(sumaz, suma)
        #res=np.sqrt(suma/sumaz)
        res=np.sqrt((suman+sumae)/sumazz)
        print(res, "****")
        return res


    def estaciones_analisis(self, vec, estacion):
        print("estacion analizado: ", estacion)
        if len(vec)==3:
            for i in range(len(vec)):
                #print(i)
                # Cargar las formas de onda de las tres componentes (N, E, Z)
                archivo_sac_e = vec[0]
                archivo_sac_n = vec[1]
                archivo_sac_z = vec[2]
                # # Cargar las formas de onda de las tres componentes (N, E, Z)
                # archivo_sac_n = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHN.SAC"
                # archivo_sac_e = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHE.SAC"
                # archivo_sac_z = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.COMA.ZA.HHZ.SAC"
            print("archivo_sac_e", archivo_sac_e)
            print("archivo_sac_n", archivo_sac_n)
            print("archivo_sac_z", archivo_sac_z)

            traza_n = obspy.read(archivo_sac_n)[0]
            traza_e = obspy.read(archivo_sac_e)[0]
            traza_z = obspy.read(archivo_sac_z)[0]

            # Calcular la relación NE/Z
            relacion_ne_z = self.calcular_relacion_ne_z(traza_n, traza_e, traza_z)
            
            return relacion_ne_z  
    def main(self, estaciones):

        file="/home/carlos/Documentos/CrossQuake-Data/tremores/"
        file22="/home/carlos/Documentos/CrossQuake-Data/"
        archivos=os.listdir(file)
        archivos=sorted(archivos)
        self.path=[]
        path11=[]
        self.promedios=[]
        self.promedios1=[]

        cont_sta=np.zeros(len(estaciones))
        sitio_sta=np.zeros(len(estaciones))
        prom_sta=   np.zeros(len(estaciones))

        for i in archivos:
            ruta0=  file+i
            self.path.append(ruta0)
            path11.append(i)

        ruta_guardar_datos=file+'site_effect'
        if not os.path.exists(ruta_guardar_datos):
            os.makedirs(ruta_guardar_datos)
        ruta00=''
        for i in range(len(self.path)):
            #print (self.path[i])
            ruta00=self.path[i]
            fo = open(ruta_guardar_datos+'/'+path11[i]+'.txt', 'a')
            if os.path.isdir(ruta00):
                arch = os.listdir(self.path[i])
                arch=sorted(arch)
                remove_esta=[]
                cont=0
                suma=0
                suma1=0

                for estacion1 in range(len(estaciones)):
                    
                    vec=[]
                    for archivo in arch:
                    

                    
                        if archivo.rfind(estaciones[estacion1]) != -1 :
                            esta=estaciones[estacion1]
                            
                            path=(ruta00+'/'+archivo)
                            vec.append(path)
                                

                    if len(vec)!=0:
                        relacion=self.estaciones_analisis(vec, estaciones[estacion1])
                        
                        if relacion != None:
                            print("relacion_ne_z", round(relacion,1))
                            fo.write(estaciones[estacion1]+'\t\t'+str(round(relacion, 1))+'\n')
                            if round(relacion, 1)<5:
                                cont_sta[estacion1]=cont_sta[estacion1]+1
                                sitio_sta[estacion1]=sitio_sta[estacion1]+relacion
        

            print("\n")
        fo.close()
        fo = open(ruta_guardar_datos+'/'+'estaciones_sitio.txt', 'a')
        for esta in range(len(estaciones)):
            prom_sta[esta]=sitio_sta[esta]/cont_sta[esta]
            fo.write(estaciones[esta]+'\t\t'+str(round(prom_sta[esta], 1))+'\n')
        fo.close()


                            

                    
                        
                        
                    
                        
       
        


        

inicio=EfectoSitio()
estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
for i in range(56):

    if i<9:
        estaciones.append('MA0'+str(i+1))
    else:
        estaciones.append('MA'+str(i+1))

inicio.main(estaciones)
import obspy
import numpy as np
import matplotlib.pyplot as plt

def calcular_relacion_ne_z(tr_n, tr_e, tr_z):
    # Calcular la relación NE/Z
    relacion_ne_z = np.sqrt(tr_n.data**2 + tr_e.data**2) / np.abs(tr_z.data)
    
    return relacion_ne_z

# Cargar las formas de onda de las tres componentes (N, E, Z)
archivo_sac_n = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.ALPI.ZA.HHN.SAC"
archivo_sac_e = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.ALPI.ZA.HHE.SAC"
archivo_sac_z = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.ALPI.ZA.HHZ.SAC"

traza_n = obspy.read(archivo_sac_n)[0]
traza_e = obspy.read(archivo_sac_e)[0]
traza_z = obspy.read(archivo_sac_z)[0]

# Calcular la relación NE/Z
relacion_ne_z = calcular_relacion_ne_z(traza_n, traza_e, traza_z)

# Calcular el valor promedio de la relación NE/Z
valor_promedio_relacion_ne_z = np.mean(relacion_ne_z)



print(f'El valor promedio de la relación NE/Z es: {valor_promedio_relacion_ne_z:.2f}')

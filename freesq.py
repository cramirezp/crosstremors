import numpy as np
from obspy import read
import matplotlib.pyplot as plt
import math
# Leer un archivo sismográfico (reemplaza "tus_datos.mseed" con el nombre de tu archivo)
#st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.05.10.21/2006.05.10.21.MAZE.HHZ.SAC")
st = read("/home/carlos/Documentos/CrossQuake-Data/tremores/2006.04.08.21/2006.04.08.21.MORA.HHZ.SAC")
# Seleccionar una traza del conjunto de datos
trace = st[0]

# Parámetros para el cálculo de la frecuencia de esquina
t_start = 0  # tiempo de inicio en segundos
t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
dt = 1 / trace.stats.sampling_rate

# Calcular la transformada de Fourier
freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas

# Tomar solo la parte positiva de la transformada
freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

# Calcular la densidad espectral de potencia
power_spectrum = np.abs(ampl_pos) ** 2

# Encontrar la frecuencia de esquina (índice del 90% de la potencia acumulada)
cumulative_power = np.cumsum(power_spectrum) / np.sum(power_spectrum)
corner_frequency_index = np.argmax(cumulative_power >= 0.9)

# Convertir el índice a frecuencia en Hz
corner_frequency = freq_pos[corner_frequency_index]


# Establecer un límite inferior en la frecuencia para graficar
lower_freq_limit = 1.0  # Ajusta según tus necesidades

# Suavizar el power_spectrum con un filtro de media móvil
window_size = 500  # Tamaño de la ventana del filtro de media móvil
power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')

# Identificar la frecuencia máxima y mínima
max_frequency_index = np.argmax(power_spectrum_smooth)
min_frequency_index = np.argmin(power_spectrum_smooth)

# Convertir los índices a frecuencias en Hz
max_frequency = freq_pos[max_frequency_index]
min_frequency = freq_pos[min_frequency_index]


# Calcular las coordenadas del segundo punto para la línea a 135 grados
angle_135 = 135 * np.pi / 180  # Convertir 135 grados a radianes
second_point_x = np.max(freq_pos)  # La coordenada x es la frecuencia máxima
second_point_y = power_spectrum_smooth[max_frequency_index]  # La coordenada y es el valor del espectro en la frecuencia máxima




for i in range(len(freq_pos)):
	if freq_pos[i]>= lower_freq_limit:
		indice=i
		break

print(indice)
freq_pos=freq_pos[indice:len(freq_pos)]
power_spectrum_smooth=power_spectrum_smooth[indice:len(power_spectrum_smooth)]




# Crear listas para almacenar los valores de frecuencia y densidad espectral
frequencies = []
power_values = []

# Iterar sobre los valores y almacenarlos en las listas
for freq_value, power_value in zip(freq_pos, power_spectrum_smooth):
    frequencies.append(freq_value)
    power_values.append(power_value)

# Convertir las listas a arrays de NumPy
frequencies = np.array(frequencies)
power_values = np.array(power_values)

# Aplicar logaritmo a los arrays
log_frequencies = np.log10(frequencies)
log_power_values = np.log10(power_values)

st_crece=0
st_decre=0
contc=0
contd=0



for i in range(len(log_power_values)):
	if log_power_values[i]<= log_power_values[i+1]:

		#if st_crece==1 and st_decre==1 and contc>200 and contd>500:
		if st_crece==1 and st_decre==1 and contc>300 and contd>500:
			
			punto_int= log_power_values[i]
			break
		else:
			st_crece=1
			contc+=1
			#print(log_power_values[i], "-- Crece" )
	else:
		st_decre=1
		contd+=1
		#print(log_power_values[i], "-- Decrece" )

		st_crece=0


print("punto_int ", punto_int)
puntos_int=[]
indice_int=[]
fin=len(log_frequencies)
#fin=log_frequencies[fin]
x_aux=np.arange(0, fin, 0.0001)

for i in range(len(log_power_values)):
		if round(log_power_values[i],2)== round(punto_int, 2):
			puntos_int.append(log_power_values[i])
			indice_int.append(i)	
#print(puntos_int)
#print(indice_int, "indices")
x11=0
y11=0
for i in (indice_int):
	#print(log_frequencies[i])
	#plt.scatter(log_frequencies[i], log_power_values[i], color='red', label='Punto (1, 2)')
	x11=log_frequencies[i]
	y11=log_power_values[i]
	x0=i

print("x11: ", x11, " y11: ", y11)
fin=len(log_frequencies)
print("valor final  ", x0)
print(len(log_frequencies))
x00=0
x=0
y2=0
nn=len(log_frequencies)
for j in range(1):
	print(j)
	j=1000
	if j>= x0:
		print("entra al if***")
		x1=round(log_frequencies[j], 2)
		y1=round(punto_int, 2)
		# Calcular la pendiente
		m = -1

		# Calcular el término independiente (b) usando la ecuación de la recta
		b = y1 - m * x1
		print("nn ", nn-1, " valor ", log_frequencies[nn-1])
		# Crear un conjunto de puntos para la recta
		x = np.linspace(0, log_power_values[nn-1], 100)
		y2 = m * x + b
		status_cruce=False
		
		n=len(y2)
		for jj in range(n):
			
			if status_cruce==False:
				for ji in range(len(log_power_values)):
					if round(y2[jj],2) == round(log_power_values[ji],2):
						print("con j= ", j, " se encuentran en la recta")
						status_cruce=True
						break
						
				if status_cruce == True:
					break
		if status_cruce == False:
			x00=j
			break


xi=log_frequencies[x00]
yi=log_power_values[x00]
print(xi, " ------ ", yi)


plt.plot(log_frequencies, log_power_values)
plt.plot(x,y2)
plt.scatter(xi, yi, color='red')
#plt.scatter(x1, y1, color='black')

plt.axhline(y=punto_int, color='blue', linestyle='--')





print(f"Frecuencia Máxima: {max_frequency:.2f} Hz")
print(f"Frecuencia Mínima: {min_frequency:.2f} Hz")
#print(f"Punto de Intersección: ({intersection_x:.2f} Hz, {intersection_y:.2f})")


# Graficar el espectro de potencia suavizado
# plt.loglog(freq_pos[freq_pos >= lower_freq_limit], power_spectrum_smooth[freq_pos >= lower_freq_limit], label='Densidad Espectral de Potencia Suavizada')
#plt.axvline(x=corner_frequency, color='red', linestyle='--', label=f'Frecuencia de Esquina: {corner_frequency:.2f} Hz')
# plt.scatter([max_frequency, min_frequency], [power_spectrum_smooth[max_frequency_index], power_spectrum_smooth[min_frequency_index]], color='green', label='Frecuencias Máxima y Mínima')
# plt.xlabel("Frecuencia (Hz)")
# plt.ylabel("Densidad Espectral de Potencia")
# plt.title("Espectro de Potencia (Parte Positiva) Suavizado con Frecuencia de Esquina (Log-log)")
# plt.legend()
plt.show()
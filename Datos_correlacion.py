#proyecto de tesis de Phd
#Desarrollado por Carlos Ramirez Pina
#====================Librerias===================================================
import sys

from OpenFile_multiple import *
from scross import *
import decimal
import random
import math
from Guardar_datos import *

from Lectura_file import *
#from Rutas import *
from Pos_Analisis_Resultados import *
from ReName import *
import os
from shutil import rmtree
#====================Librerias===================================================

class Datos_correlacion():
	print ("welcome to system for egenerate calog of the events seismic")
	filename = ""
	pathArchivoDE=None
	pathArchivoDF=None
	def pathfileDE(self):
		Tipo=self.pathRepo
		inicia_guardando=Guardar_datos()


		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo, self.Channel)
		#print ("self.filename----", self.filename[1])
		self.ruta_repository=self.filename[1]
		return(self.filename[0])
	def pathfileDF(self):
		Tipo=self.pathCat
		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo, self.Channel)
		return(self.filename[0])
	def abrirArchivo(self):
		#print ':)****'
		
		archivo =self.pathfileDE()
		#self.etiquetaPath.config(text="Select files:"+str(len(archivo)))
		self.pathArchivoDE=archivo
		#print  self.pathArchivoDE
		
	def abrirArchivoFile(self):

		archivo =self.pathfileDF()
		#self.etiquetaPath1.config(text="Select files:"+str(len(archivo)))
		self.pathArchivoDF=archivo
		#print  self.pathArchivoDF

	def abrirArchivoStation(self):

		
		path_sta=self.path_sta
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		#print self.estaciones

		#archivo =self.pathfileDF()
		#self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF



	def trabajar(self):
		#print  ":)"
		dias_analisis=[]
		
		self.year_an=self.year
		self.fm=40
		respuesta=self.validar()
		if respuesta == True:
			#============================ Se indica path para guardar resultados ===============================
			inicia=Guardar_datos()
			#ipo="Select file for salve results"
			#ruta_guardar_datos_end=inicia.guardar_directorio(Tipo)
			ax_pat=os.getcwd()
			ruta_guardar_datos_end=ax_pat+"/"+self.code+"L"
			ruta_guardar_datos=ruta_guardar_datos_end+"/results_pre"
			print ('==============================================')
			print (" path save files", ruta_guardar_datos)

			if not os.path.exists(ruta_guardar_datos):
				os.makedirs(ruta_guardar_datos)

			print ('==============================================')
			inicia_xcross=scross()
			estaciones=self.estaciones
			#estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
			
			for i in range(len(self.pathArchivoDE)):

				for ii in self.estaciones:
					if self.pathArchivoDE[i].find(ii) !=-1:
						self.estacion_1=ii

				#=========================================
				for j in range(len(self.pathArchivoDF)):

					for jj in self.estaciones:
						if self.pathArchivoDF[j].find(jj) != -1:
							self.estacion_2=jj
					if self.estacion_1==self.estacion_2:
						dia_aux=inicia_xcross.main( self.pathArchivoDE[i], self.pathArchivoDF[j], ruta_guardar_datos, self.estaciones, self.year_an, self.Channel, self.fm, self.code)
						#verifica los dias analizados por el sistema y lo guarda en el vector dias analisis
						if dia_aux not in dias_analisis:
							if dia_aux!= None:
								dias_analisis.append(dia_aux)

			

		print ("--- ", dias_analisis, "----")
		inicia_pos_analisis=Pos_Analisis_Resultados()
		#print "++", ruta_guardar_datos_end, "++", d
		for d in dias_analisis:
			print ("++", ruta_guardar_datos_end, "++", d)
			inicia_pos_analisis.main(ruta_guardar_datos_end,ruta_guardar_datos, d, self.estaciones, self.ruta_repository, self.year_an, self.fm, self.Channel, self.code)
		#inicia=ClasificaPrueba()
		#inicia.main(self.pathArchivoDE, self.pathArchivoDF)
		inicia_rename=ReName()
		inicia_rename.main(self.year_an, self.estaciones, ruta_guardar_datos, ruta_guardar_datos_end, dias_analisis)
		#rmtree(ruta_guardar_datos_end)



	

	def validar(self):
		valido = True
		##print  'entra'

		
		return valido


	def habilitarManual(self):
		if len(self.b)!=0:
			self.botonGuardar.config(state=NORMAL)



	def habilitarDataSet(self, long):
		if long !=0:
			self.entrada_texto.config(state=NORMAL)
			self.entradaIter.config(state=NORMAL)
			self.entradaMSE.config(state=NORMAL)
			self.botonEntrenar.config(state=NORMAL)


		# cadena = "Factor de correccion: " + str(alfa) + "\n" + "Error minimo esperado: " + str(errorminimo) + "\n" + "Error Promedio: " + str(errorpromedio) + "\n"+ "Iteracion: " + str(iteracion) + "\n" + "Error minimo: " + str(minimo) + "\n" + "r1: " + str(ww11) + "\n" + "r2: " + str(uu) + "\n" + "bias: " + str(bb1) + "\n"	+ "bias2:"  + str(bb3) + "\n"
		# result.config(state=NORMAL)
		# result.insert(INSERT, cadena)
		# result.config(state=DISABLED)
	def cambiar_stringvar(nuevotexto,stringvar):
		stringvar.set(nuevotexto)



	def main(self):

		print("Input of data fpr correlation local")
		self.year=input("Input year of analisys (YYYY):  ")
		try:
			self.year=int(self.year)
			if (self.year)>=1954:
				print("year is:  ", self.year)
				status_year=True
		except Exception as e:
			print("format incorrect")
			status_year=False
		
			while status_year == False:
				self.year=input("Input year of analisys (YYYY):  ")
				try:
					self.year=int(self.year)
					if (self.year)>=1954:
						print("year is:  ", self.year)
						status_year=True
				except Exception as e:
					print("format incorrect")
					status_year=False

		archivo=input("Input path catalog (masters)")
		

		try:
			if (os.path.isdir(archivo)) :
				self.pathCat=archivo
				status_df=True
		except Exception as e:
			print("Error, format incorrect")
			status_df=False
		while status_df==False:
			print("path not is a dir")
			archivo=input("Input path catalog (masters)")
		

			try:
				if (os.path.isdir(archivo)) :
					self.pathCat=archivo
					status_df=True
			except Exception as e:
				print("Error, format incorrect")
				status_df=False


		archivo=input("Input path repository")
		

		try:
			if (os.path.isdir(archivo)) :
				self.pathRepo=archivo
				status_de=True
		except Exception as e:
			print("Error, format incorrect")
			status_de=False
		while status_de==False:
			print("path not is a dir")
			archivo=input("Input path repository ")
		

			try:
				if (os.path.isdir(archivo)) :
					self.pathRepo=archivo
					status_de=True
			except Exception as e:
				print("Error, format incorrect")
				status_de=False
		


		self.code=input("Input FDSN code:  ")
		try:
			print("FDSN code is: ", self.code)
			self.code = self.code.upper()
			status_code=True
		except Exception as e:
			print("error input FDSN code")
			status_code=False

		while status_code == False:
			self.code=input("Input FDSN code:  ")
			try:
				print("FDSN code is: ", self.code)
				self.code = self.code.upper()
				status_code=True
			except Exception as e:
				print("error input FDSN code")
				status_code=False


		print("only enter the first two characters (e.g. HH, HB, LL..)")
		self.Channel=input("Input name Channel:\t  ")
		#print("only enter the first two characters (e.g. HH, HB, LL..)")
		try:
			print("Name Channel is:  ", self.Channel)
			self.Channel = self.Channel.upper()
			status_channel=True
		except Exception as e:
			print("error input name Channel")
			status_channel=False

		while status_channel == False:
			print("only enter the first two characters (e.g. HH, HB, LL..)")
			self.Channel=input("Input name Channel:\t  ")
			
			try:
				print("Name Channel is: ", self.Channel)
				self.Channel = self.Channel.upper()+"?"
				status_code=True
			except Exception as e:
				print("error input name Channel")
				status_code=False

		self.path_sta=input("Input path file stations")

		

		try:
			if (os.path.isfile(self.path_sta)) :
				status_sta=True
		except Exception as e:
			print("Error, format incorrect")
			status_sta=False

		while status_sta== False:
			self.path_sta=input("Input path file stations")

		

			try:
				if (os.path.isfile(self.path_sta)) :
					status_sta=True
			except Exception as e:
				print("Error, format incorrect")
				status_sta=False


		self.mostrarInfo()
	def mostrarInfo(self):
		os.system("clear")
		print ("informayion input")
		print("year is:\n ", self.year)
		print("Path catalog master is:\n ", self.pathCat)
		print("Path catalog repository is:\n ", self.pathRepo)
		print("Path file station is:\n ", self.path_sta)
		print("Channel is:\n ", self.Channel)
		print("Code is:\n", self.code)
		print("\n *******************************")
		resp=input("right information? (Y/N)")

		if resp == "Y" or resp =="y" or resp== "yes":
			self.abrirArchivo()
			self.abrirArchivoFile()
			self.abrirArchivoStation()
			self.trabajar()
		else:
			print("1.-input information")
			print("2.-cancelar")
			choice=input("Select choice (1-2):\n")

			try:
			
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=3:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 2\n")

			except:
				os.system("clear")
				choice=0
				print("format incorrect, please enter number between 1 and 2\n")


			while choice <=0 or choice>=3:

			
				print("1.-input information")
				print("2.-cancelar")
				choice=input("Select choice (1-2):\n")
				try:
				
					choice=int(choice)
					print ("choice select: ",choice)
					if choice <=0 or choice>=3:
						os.system("clear")
						print("Choice incorrect, please select choice between 1 and 2\n")
				except:
					os.system("clear")
					choice=0
					print("Choice incorrect, please select choice between 1 and 2\n")

			if choice ==1:
				self.main()
			else:
				self.cancelar()
		
	def cancelar(self):
		os.system("clear")
	




		'''

		valor = "" #para el inicio los entry tenga b

		self.app = Tk()
		self.app.title('Input of data for correlation')
		self.app.geometry("736x200")
		self.app.maxsize(736, 200)

		#VP -> VENTANA PRINCIPAL
		vp = Frame(self.app)
		
		vp.grid(column=0, row=0, padx=(50,50), pady=(10,10)) #posicionar los elementos en tipo matriz, padx es para margen
		vp.columnconfigure(0,weight=1)  #tamanio relativo a las columnas
		vp.rowconfigure(0,weight=1)


		#DATOS ENTRADA POR PATH
		etiquetaDE = Label(vp,text="Set data: ")
		etiquetaDE.grid(column=0, row=0) #especificar en la final y columna 
		
		boton = Button(vp, width=21, text="Select for day (repository)", command=self.abrirArchivo)
		boton.grid(column=2,row=0)

		etiquetaDF = Label(vp,text="Select set data ")
		etiquetaDF.grid(column=0, row=2) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select catalog (masters)", command=self.abrirArchivoFile)
		boton.grid(column=2,row=2)
 	

		etiquetaDF = Label(vp,text="Select set stations ")
		etiquetaDF.grid(column=0, row=3) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select file stations", command=self.abrirArchivoStation)
		boton.grid(column=2,row=3)

		etiqueta = Label(vp,text="year of analysis: ")
		etiqueta.grid(column=0, row=4) 
		self.entrada_texto = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.entrada_texto.grid(column=1,row=4)

		self.etiquetaPath = Label(vp,text=" ")
		self.etiquetaPath.grid(column=1, row=0) 

		self.etiquetaPath1 = Label(vp,text=" ")
		self.etiquetaPath1.grid(column=1, row=2) 
		
		self.etiquetaPath2 = Label(vp,text=" ")
		self.etiquetaPath2.grid(column=1, row=3) 
		
		# #EJECUCION
		boton = Button(vp, width=20, text="Run", command=self.trabajar)
		boton.grid(column=1,row=8)


	


		botonSalir = Button(vp, width=20, text="Quit", command=self.app.destroy)
		botonSalir.grid(column=2,row=22)
		


		#CREDITOS
		etiqueta = Label(vp,text="Elaborado por: CARLOS RAMIREZ PINA")
		etiqueta.grid(column=1, row=40) 
		self.app.mainloop()
		'''

#Inicia_D_C=Datos_correlacion()
#Inicia_D_C.main()

